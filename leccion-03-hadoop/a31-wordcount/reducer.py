#!/opt/anaconda/bin/python

import sys

wordCount = 0
oldWord = None

# bucle por todos los datos ... 
# en el mismo formato que los emite la funcion map
#
# OJO! se reciben todos los datos por sys.stdin
# y por eso hay que hacer el bucle y distinguir las distintas keys
#
for line in sys.stdin:
    data_mapped = line.strip().split("\t")

    thisWord, count = data_mapped

    # emito resultado cuando se acabaron los registros de la clave actual
    if oldWord and oldWord != thisWord:
        print oldWord, "\t", wordCount
        wordCount = 0

    oldWord = thisWord
    wordCount += 1

# emito el resultado de la ultima clave
if oldWord != None:
    print oldWord, "\t", wordCount

