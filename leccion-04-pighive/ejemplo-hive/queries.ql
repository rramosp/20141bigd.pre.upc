create table usuarios
(nombre STRING, ciudad STRING, edad BIGINT)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
STORED AS TEXTFILE;

LOAD DATA LOCAL INPATH 'personas.txt' OVERWRITE INTO TABLE usuarios;

SELECT count(*) from usuarios where edad>40;

SELECT nombre from usuarios WHERE edad>20 and edad<40;



