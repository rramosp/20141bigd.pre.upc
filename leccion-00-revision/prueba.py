import numpy as np
from milibreria import *

# los comentarios con '#'
r1 = mifuncion(10,3)
r2 = mifuncion(np.array([[1,2,3],[4,5,6]]))

print r1
print r2
print "----"

# observa la indentacion
for i in np.arange(0,10):
    print i, mifuncion(i)

print "--- fin ---"

